package com.example.demo.controlador;

import com.example.demo.Modelo.ModeloProductos;
import com.example.demo.Modelo.ModeloUsuario;
import com.example.demo.servicio.ServicioDatos;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("${api.version}/productos")
public class ControladorUsuariosProducto {

    @Autowired
    private ServicioDatos servicioDatos;

    @GetMapping("/{idProducto}/usuarios")
    public ResponseEntity obtenerUsuariosProducto(@PathVariable int idProducto) {
        try {
            return ResponseEntity.ok(this.servicioDatos.obtenerUsuarioProducto(idProducto));
        } catch (Exception x) {
            x.printStackTrace();
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/{idProducto}/usuarios/{idUsuario}")
    public ResponseEntity obtenerUsuarioProducto(@PathVariable int idProducto, @PathVariable int idUsuario){
        final ModeloUsuario u = this.servicioDatos.obtenerUsuarioProducto(idProducto, idUsuario);
        return (u == null)
                ? new ResponseEntity(HttpStatus.NOT_FOUND)
                : ResponseEntity.ok(u);
    }

    @PostMapping("/{idProducto}/usuarios")
    public ResponseEntity agregarUsuarioProducto(@PathVariable int idProducto, @RequestBody ModeloUsuario usuario){
        final ModeloProductos p = this.servicioDatos.obtenerProductoPorId(idProducto);
        if (p == null)
            return new ResponseEntity(HttpStatus.NOT_FOUND);
           return ResponseEntity.ok(this.servicioDatos.AgregarUsuarioProducto(idProducto, usuario));
    }

    @PutMapping("/{idProducto}/usuarios/{idUsuario}")
    public ResponseEntity ModificarUsuarioProducto(@PathVariable int idProducto, @PathVariable int idUsuario,
                                                   @RequestBody ModeloUsuario usuario){
        this.servicioDatos.actualizarUsuarioProducto(idProducto, idUsuario, usuario);
        return new ResponseEntity("Actualizado",HttpStatus.OK);
    }

    @DeleteMapping("/{idProducto}/usuarios/{idUsuario}")
    public ResponseEntity borrarUsuarioProducto(@PathVariable int idProducto, @PathVariable int idUsuario){
        this.servicioDatos.borrarUsuarioProducto(idProducto, idUsuario);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}



