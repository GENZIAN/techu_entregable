package com.example.demo.controlador;


import com.example.demo.Modelo.ModeloProductos;
import com.example.demo.servicio.ServicioDatos;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("${api.version}/productos")
public class ControladorProductos {

    @Autowired
    private ServicioDatos servicioDatos;

    @GetMapping("/")
    public ResponseEntity obtenerUnProducto() {
       return ResponseEntity.ok(this.servicioDatos.obtenerProductos());
    }

    @PostMapping("/")
    public ResponseEntity crearProducto(@RequestBody ModeloProductos producto){
        final ModeloProductos p = this.servicioDatos.agregarProducto(producto);
        return ResponseEntity.ok(p);
    }

    @GetMapping("/{id}")
    public ResponseEntity obtenerUnProducto(@PathVariable int id) {
        final ModeloProductos p = this.servicioDatos.obtenerProductoPorId(id);
        if(p !=null) {
            return ResponseEntity.ok(p);
        }
           return new ResponseEntity(HttpStatus.NOT_FOUND);
    }

    @PutMapping("/{id}")
    public ResponseEntity actualizaUnProducto(@PathVariable int id,
                                              @RequestBody ModeloProductos producto) {
        this.servicioDatos.actualizarProducto(id, producto);
        return new ResponseEntity("Actualizado",HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity borrarUnProducto(@PathVariable int id){
        this.servicioDatos.borrarProducto(id);
        return new ResponseEntity("Eliminado", HttpStatus.NO_CONTENT);
    }

    @PatchMapping("/{id}")
    public ResponseEntity patchprecio(@PathVariable int id,
                                      @RequestBody ModeloProductos producto) {
        this.servicioDatos.patchProducto(id,producto);
        return new ResponseEntity("Actualizado",HttpStatus.OK);
    }
}
