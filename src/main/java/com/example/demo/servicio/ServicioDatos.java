package com.example.demo.servicio;


import com.example.demo.Modelo.ModeloProductos;
import com.example.demo.Modelo.ModeloUsuario;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

//CRUD de Productos
//Create
//Read (Collection un Producto)
//Update
//Delete

@Component // Le dice a Spring que maneje objetos de esta clase
public class ServicioDatos {
    private final AtomicInteger secuenciaIdsProductos
            =new AtomicInteger(0);
               private final AtomicInteger secuenciaIdsUsuarios
            =new AtomicInteger(0);
    private final List<ModeloProductos> productos
            =new ArrayList<ModeloProductos>();

    //CREATE
    public ModeloProductos agregarProducto(ModeloProductos producto) {
        producto.setId(this.secuenciaIdsProductos.incrementAndGet());
        this.productos.add(producto);
        return producto;
    }

    public ModeloUsuario AgregarUsuarioProducto(int idProducto, ModeloUsuario usuario) {
        usuario.setId(this.secuenciaIdsUsuarios.incrementAndGet());
        this.obtenerProductoPorId(idProducto).getUsuarios().add(usuario);
        return usuario;
    }

    //READ Collecion
    public List<ModeloProductos> obtenerProductos(){
        return Collections.unmodifiableList(this.productos);
    }
    public List<ModeloUsuario> obtenerUsuarioProducto(int idProducto) {
        return Collections.unmodifiableList(this.obtenerProductoPorId(idProducto).getUsuarios());
    }

    //READ elemento
    public ModeloProductos obtenerProductoPorId(int id){
        for(ModeloProductos p: this.productos){
            if(p.getId() == id)
                return p;  //OJO se puede modificar desde afuera
            }
        return null;
    }

    public ModeloUsuario obtenerUsuarioProducto(int Idproducto, int Idusuario){
        final ModeloProductos p= this.obtenerProductoPorId(Idproducto);
        if(p == null)
            return null;
        for(ModeloUsuario u: p.getUsuarios()){
            if(u.getId() == Idusuario){
                return u; // OJO se puede modificar desde afuera
            }
        }
        return null;
    }
    
    //UPDATE
    public boolean actualizarProducto(int id, ModeloProductos producto) {
        for(int i=0; i < this.productos.size(); ++i){
            if(this.productos.get(i).getId() == id){
             producto.setId(id);
             this.productos.set(i, producto);
             return true;
            }
        }
        return false;
    }

    //PATCH
    public boolean patchProducto(int id, ModeloProductos producto) {
        for(int i=0; i < this.productos.size(); ++i){
            if(this.productos.get(i).getId() == id){
               producto.setId(id);
               this.productos.set(i,producto);
               return true;
            }
        }
        return false;
    }


    public boolean actualizarUsuarioProducto(int Idproducto, int Idusuario, ModeloUsuario usuario){
        List<ModeloUsuario> usuarios = this.obtenerProductoPorId(Idproducto).getUsuarios();
        for(int i=0; i < usuarios.size(); ++i) {
            if(usuarios.get(i).getId() == Idusuario) {
                usuario.setId(Idusuario);
                usuarios.set(i, usuario);
                return true;
            }
        }
        return false;
    }

    //DELETE
    public boolean borrarProducto(int id){
        for(int i=0; i < this.productos.size(); ++i) {
            if (this.productos.get(i).getId() == id) {
                this.productos.remove(id);
                return true;
            }
        }
        return false;
    }

    public boolean borrarUsuarioProducto(int idProducto, int idUsuario) {
        final ModeloProductos producto = this.obtenerProductoPorId(idProducto);
        if(producto == null)
            return false;
        final List <ModeloUsuario> usuarios = producto.getUsuarios();
        for(int i=0; i < usuarios.size(); ++i) {
            if(usuarios.get(i).getId() == idUsuario){
                usuarios.remove(i);
                return true;
            }
        }
        return false;
    }
}

